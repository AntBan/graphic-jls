// Nada: mostrará todo el contenido del directorio actual
// Fichero: mostrará el fichero
// Directorio: mostrará el contenido del directorio (a menos que use -d)
// Ficheros: archivo (texto, script, código fuente...), directorio (carpetas)

import gnu.getopt.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.UserPrincipal;
import java.text.SimpleDateFormat;
import java.util.*;

public abstract class gjls implements Path {
	
	private static JFrame frame = new JFrame ("gjls");
	private static JPanel panelBase = new JPanel (new GridLayout (2, 1));
	private static JPanel panelStdout = new JPanel (new GridLayout (1, 1));
	private static JPanel panelStdin = new JPanel (new GridLayout (1, 2));
	private static JTextArea stdout = new JTextArea ();
	private static JScrollPane scroll = new JScrollPane (stdout, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	private static JTextField stdin = new JTextField();
	private static JButton mostrar = new JButton ("Mostrar");


	private static final String RESET = "\u001B[0m";
	private static final String BLUE = "\u001B[34m";

	private static boolean a_opt = false; // Se mostrarán los ficheros ocultos
	private static boolean d_opt = false; // Se mostrará el nombre del directorio
	private static boolean l_opt = false; // Listado largo del fichero
	private static boolean r_opt = false; // Mostrar los ficheros en orden inverso



	/** 
	 * Muestra por stdout el error producido y finaliza el programa
	 * @param err String con el mensaje de error
	 */
	public static void error (String err) {
		stdout.append (err);
	}



	/** 
	 * DEBUG: muestra por stdout un array dado
	 * @param arrLis Array de ficheros
	 */
	public static void debugArray (ArrayList<File> arrLis) {

		for (File f: arrLis) {
			System.out.print ("\t"+ f.getName() +"\n");
		}
	}



	/** 
	 * DEBUG: muestra por stdout un string dado
	 * @param cadena String que contiene el nombre de la variable y su valor
	 */
	public static void debugPrint (String cadena) {
		System.out.println (cadena);
	}



	/** 
	 * Muestra la ayuda del programa
	 */
	public static void mostrarHelp() {
		stdout.append ("Uso: jls [OPCIÓN]... [FICHERO]...\n"
						+"Lista información sobre los FILEs (directorio actual por defecto).\n"
						+"Ordena los ficheros según el orden natural\n\n"
						+"  -a\tno ignora ficheros que comiencen por .\n"
						+"  -c\tcolorea el output\n"
						+"  -d\tmuestra el directorio en si mismo y no el contenido\n"
						+"  -h\tmuestra esta ayuda y finaliza la ejecución\n"
						+"  -l\tformato de listado largo\n"
						+"  -r\tinvierte el orden de mostrado\n"
						+"  -R\tmuestra directorios recursivamente\n\n"
						+"BNG joreutils");
	}



	/**
	 * Calcula los permisos de un fichero dado al nivel de el usuario
	 * propietario
	 * @param fichero fichero (archivo o directorio)
	 * @return String con los permisos (rwx)
	 */
	public static String getPermisos (File fichero) {
		char[] permisos = new char[3];

		permisos[0] = fichero.canRead() ? 'r' : '-';
		permisos[1] = fichero.canWrite() ? 'w' : '-';
		permisos[2] = fichero.canExecute() ? 'x' : '-';

		return new String (permisos);
	}



	/**
	 * Calcula el número de links o directorios de un fichero. Un directorio
	 * tendrá por defecto 2 por ". .."
	 * @param fichero Fichero (archivo o directorio)
	 * @return el número de links o directorios de un fichero
	 */
	public static int getLinksDirectorios (File fichero) {
		int num;
		File[] subFicheros = fichero.listFiles();
		
		if (fichero.isDirectory()) {
			num = 2;

			// Contador de subdirectorios
			for (File f: subFicheros) {
				if (f.isDirectory()) num++;
			}
		}
		else {
			num = 1;	// Es un fichero
		}

		return num;
	}



	/**
	 * Usuario propietario de un fichero dado
	 * @param fichero fichero (archivo o directorio)
	 * @return el nombre del usuario propietario
	 */
	public static String getPropietario (File fichero) {
		UserPrincipal usuario = null;

		try {
			Path fichPath = Paths.get(fichero.getName());	// Devuelve la ruta del fichero
			usuario = Files.getOwner (fichPath);
		}
		catch (IOException ioe) {
			error ("getPropietario(): "+ ioe.getMessage());
		}

		return usuario.getName();
	}



	/**
	 * Calcula la última vez que un fichero dado fue modificado
	 * @param fichero fichero (archivo o directorio)
	 * @return fecha de la última modificación del fichero
	 */
	public static String getUltimaModificacion (File fichero) {
		Date fecha = new Date (fichero.lastModified());
		SimpleDateFormat sdf = new SimpleDateFormat ("MMM d HH:m");

		return sdf.format (fecha);
	}



	/**
	 * Método que muestra las propiedades del fichero
	 * @param fichero Fichero (archivo o directorio)
	 * @return un String con todos los campos del listado largo
	 */
	public static String listadoLargo (File fichero) {
		char tipo; 			// Tipo de fichero
		String permisos; 	// Read, Write, eXecute
		int linksDirs; 		// Número de links o directorios
		String usuario; 	// Usuario propietario
		long tamanio; 		// Tamaño del fichero
		String ultimaMod;	// Última modificación

		tipo = fichero.isDirectory() ? 'd' : '-';	// Es un directorio
		permisos = getPermisos (fichero);				// Devuelve los permisos
		linksDirs = getLinksDirectorios (fichero);		// Devuelve el número de links o directorios
		usuario = getPropietario (fichero);				// Devuelve el usuario propietario
		tamanio = fichero.length();						// Devuelve el tamaño
		ultimaMod = getUltimaModificacion (fichero);	// Devuelve la última modificación

		//debugPrint (tipo +""+ permisos +" "+ linksDirs +" "+ usuario);
		return tipo +""+ permisos +" "+ linksDirs +" "+ usuario +" "+ tamanio +" "+ ultimaMod;
	}



	/** 
	 * Muestra por stdout los ficheros almacenados en "arrLis" en orden natural. 
	 * Al terminar, cierra la ejecución del programa
	 * @param arrLis Ficheros a ser mostrados
	 */
	public static void mostrarFicheros (ArrayList<File> arrLis) {

		arrLis.trimToSize();	// Ajusta el tamaño del ArrayList al contenido

		if (r_opt) { // Ordenar de forma inversa
			Collections.sort (arrLis, Collections.reverseOrder());
		}
		else {
			Collections.sort (arrLis);
		}

		for (File f: arrLis) {

			if (! f.exists()) { // Muestra el error si el fichero no existe
				error ("jls: cannot access '"+ f.getName() +"': No such file or directory");
				continue;
			}

			if (a_opt) { // Mostrar ficheros ocultos

				//if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul

				if (l_opt) {
					stdout.append (listadoLargo (f) +" ");
					//if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul
					stdout.append (f.getName() +"\n");
				}
				else {
					//if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul
					stdout.append (f.getName() +"\t");
				}
			}
			else {
				if (! (f.getName().charAt (0) == '.')) {

					//if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul

					if (l_opt) {
						stdout.append (listadoLargo (f) +" ");
						//if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul
						stdout.append (f.getName() +"\n");
					}
					else {
						//if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul
						stdout.append (f.getName() +"\t");
					}
				}
			}
			//System.out.print (RESET); // Resetear el color
		}
		stdout.append ("\n");
		//System.exit (0);
	}



	/** 
	 * Formatear stdout para mostrar el contenido de los ficheros o el nombre 
	 * los directorios (d_opt = true)
	 * @param arrLis Objeto ArrayList uno o varios directorios 
	 * @param varios Indica si hay uno o varios directorios en "arrLis"
	 */
	public static void mostrarContenido (ArrayList<File> arrLis, boolean varios) {
		ArrayList<File> subFicheros = new ArrayList<File>();

		arrLis.trimToSize();	// Ajusta el tamaño del ArrayList al contenido
		//if (a_opt) System.out.print (".\t..\t");
		
		if (d_opt) mostrarFicheros (arrLis); // Mostrar el nombre de los directorios

		if (varios) {

			for (File f: arrLis) {

				if (! f.canRead()) {
					error ("jls: cannot open directory '"+ f.getName() +"': Permission denied");
				}

				subFicheros.addAll (Arrays.asList (f.listFiles()));	// Agrega los sub ficheros

					debugPrint ("R_opt: " + R_opt);
				if (R_opt) {
					mostrarFicheros (subFicheros);
					for (File sf: subFicheros) {
						if (sf.isDirectory()) {
							stdout.append ("\n"+ sf.getName() +":\n");
							mostrarContenido ((ArrayList<File>) Arrays.asList (sf.listFiles()), true);
						}
						/*
						mostrarFicheros (subFicheros);
						if (sf.isDirectory())
							mostrarContenido (Arrays.asList (sf.listFiles()), true);
							*/
					}
				}
				else {
					stdout.append ("\n"+ f.getName() +":\n");
					//subFicheros.addAll (Arrays.asList (f.listFiles()));	// Agrega los sub ficheros
					mostrarFicheros (subFicheros);						// Muestra sub ficheros
				}

				subFicheros.clear(); 								// Limpiar los sub ficheros
			}
		}
		else {

			if (! arrLis.get (0).canRead()) {
				error ("jls: cannot open directory '"+ arrLis.get (0).getName() +"': Permission denied");
			}

			subFicheros.addAll (Arrays.asList (arrLis.get (0).listFiles()));	// Aprega los sub ficheros
			mostrarFicheros (subFicheros);										// Muestra los sub ficheros
		}
	}



	/** 
	 * Rellena "arrLis" con argumentos no-Getopt almacenados en "args". "inicioArgs" indica
	 * el comienzo de los ficheros y "opt" si hay alguna opción Getopt en el array
	 * @param arrLis Objeto ArrayList a rellenar de ficheros
	 * @param inicioArgs Posición del primer fichero en "args"
	 * @param args Array con todos los parámetros pasados a main
	 * @param opt true si hay opcines Getopt, false si no hay
	 */
	public static void arrayFicheros (ArrayList<File> arrLis, int inicioArgs, String[] args, boolean opt) {

		//debugPrint ("inicioArgs: "+ inicioArgs);
		//debugPrint ("args.length: "+ args.length);
		if (opt) {
			// Hay Getopt
			for (int cont = inicioArgs; cont <= (args.length - inicioArgs); cont++) {
				//debugPrint ("cont: "+ cont);
				arrLis.add (new File (args[cont]));
			}
		}
		else {
			// No hay Getopt
			for (int cont = inicioArgs; cont < (args.length - inicioArgs); cont++) {
				//debugPrint ("cont: "+ cont);
				arrLis.add (new File (args[cont]));
			}
		}
		//debugArray (arrLis);
	}



	/** 
	 * Devuelve los directorios almacenados en "arrLis"
	 * @param arrLis Objeto ArrayList con todos los ficheros almacenados
	 * @return objeto ArrayList relleno con directorios
	 */
	public static ArrayList<File> arrayDirectorios (ArrayList<File> arrLis) {
		ArrayList<File> directorios = new ArrayList<File>();

		for (File f: arrLis) {

			if (f.isDirectory()) {
				directorios.add (f);
			}
		}

		return directorios;
	}



	/** 
	 * Devuelve los archivos almacenados en "arrLis"
	 * @param arrLis Objeto ArrayLista con todos los ficheros almacenados
	 * @return objeto ArrayList relleno con archivos
	 */
	public static ArrayList<File> arrayArchivos (ArrayList<File> arrLis) {
		ArrayList<File> archivos = new ArrayList<File>();

		for (File f: arrLis) {

			if (! f.isDirectory()) {
				archivos.add (f);
			}
		}

		return archivos;
	}



	public static void main (String args[]) {

		frame.setDefaultCloseOperation (WindowConstants.EXIT_ON_CLOSE);
		frame.setPreferredSize (new Dimension (500, 500));
		frame.add (panelBase);

		//panelStdout.setPreferredSize (new Dimension (500, 500));
		panelBase.add (panelStdout);
		panelStdout.add (stdout);
		stdout.add (scroll);

		//panelStdin.setPreferredSize (new Dimension (10, 10));
		panelStdin.setSize (10, 500);
		panelBase.add (panelStdin);
		panelStdin.add (stdin);
		panelStdin.add (mostrar);

		frame.pack();
		frame.setLocationRelativeTo (null);
		frame.setVisible (true);

		mostrar.addActionListener (new ActionListener() {
			public void actionPerformed (ActionEvent event) {
				String[] args = stdin.getText().split (" ");

				int c;
				boolean opt; // T: hay getopt, F: no hay getopt
				String arg;
				Getopt option = new Getopt ("jls", args, "acdhlr");
				while ((c = option.getopt()) != -1) {
					switch (c) {
						case 'a':
							a_opt = true;
							break;

						case 'c':
							c_opt = true;
							break;

						case 'd':
							d_opt = true;
							break;

						case 'h':
							mostrarHelp();
							break;

						case 'l':
							l_opt = true;
							break;

						case 'r':
							r_opt = true;
							break;
					}
				}

				ArrayList<File> ficheros = new ArrayList<File>();	// ArrayList de ficheros
				File path = null;									// Path a un fichero
				int inicioArgs = option.getOptind(); 				// Primer argumento no Getopt
				opt = inicioArgs > 0 ? true : false;				// Hay o no hay getopts?
				arrayFicheros (ficheros, inicioArgs, args, opt); 	// Rellena ficheros con ficheros


				// Sin parámetros: mostrar todos los ficheros del directorio actual
				if (ficheros.size() == 0) { 
					path = new File ("./");
					
					if (d_opt) { // Muestra el nombre del directorio actual
						//System.out.println (BLUE + path.getName() + RESET);
						stdout.append (path.getName() +"\n");
						System.exit (0);
					}

					// Comprobar si se puede leer
					if (path.canRead()) { 
						ficheros.addAll (Arrays.asList (path.listFiles()));

						//mostrarFicheros (ficheros);
						if (R_opt) { // Mostrar el contenido recursivo
							mostrarContenido (ficheros, true);
						}
						else { // Mostrar el contenido de normal
							mostrarFicheros (ficheros);
						}
					}
					else {
						error ("jls: cannot open directory '.': Permission denied");
					}
				}
				// Con parámetros 1,1
				else if (ficheros.size() == 1) {

					if (ficheros.get (0).isDirectory()) {
						mostrarContenido (ficheros, false); // Mostrar el contenido de un directorio
					}
					else {
						mostrarFicheros (ficheros); // Mostrar el nombre de un archivo
					}
				}

				// 2,n ficheros
				else { 

					ficheros.trimToSize();	// Ajusta el tamaño al contenido
					ArrayList<File> directorios = arrayDirectorios (ficheros);
					ArrayList<File> archivos = arrayArchivos (ficheros);

					if (! (archivos.size() == 0)) {
						mostrarFicheros (archivos);
					}

					if (! (directorios.size() == 0)) {
						mostrarContenido (directorios, true); // Mostrar varios directorios
					}
				}
				stdin.setText ("");
			}
		});
	}
}
